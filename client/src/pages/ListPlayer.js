import { Component } from "react";
import Text from "../components/Text";

class ListPlayer extends Component {
  state = {
    username: "",
    email: "",
    experience: "",
    lvl: "",
    searchValue: "",
    players: [
      {
        username: "john",
        email: "john@lennon.com",
        experience: 999,
        lvl: 1000,
      },
      {
        username: "paul",
        email: "paul@mccartney.com",
        experience: 998,
        lvl: 1001,
      },
      {
        username: "ringo",
        email: "ringo@starss.com",
        experience: 666,
        lvl: 777,
      },
      {
        username: "george",
        email: "george@harrison.com",
        experience: 123,
        lvl: 888,
      },
    ],
  };

  findPlayer = async () => {
    var data = await this.state.players.find(
      ({ username }) => username === this.state.searchValue
    );
    console.log(data);
    this.setState({
      username: data.username,
      email: data.email,
      experience: data.experience,
      lvl: data.lvl,
    });
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    return (
      <div>
        <h1>List Player</h1>

        <Text
          fieldName="searchValue"
          placeholder="search username"
          value={this.state.searchValue}
          onChange={this.handleFieldChange}
        />

        <button onClick={this.findPlayer}>Search</button>
        <br />
        <span>Username: {this.state.username}</span>
        <br />
        <span>Email: {this.state.email}</span>
        <br />
        <span>Experience: {this.state.experience}</span>
        <br />
        <span>Level: {this.state.lvl}</span>
        <br />
        <br />
        <table>
          <tr>
            <th>No.</th>
            <th>UserName</th>
          </tr>
          {this.state.players.map((player, index) => {
            return (
              <tr>
                <td>{index + 1}</td>
                <td>{player.username}</td>
              </tr>
            );
          })}
        </table>
      </div>
    );
  }
}

export default ListPlayer;
