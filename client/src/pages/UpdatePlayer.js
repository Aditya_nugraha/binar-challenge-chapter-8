import Text from "../components/Text";
import Email from "../components/Email";
import Experience from "../components/Experience";
import Lvl from "../components/Lvl";
import { Component } from "react";

class UpdatePlayer extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    showSummary: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showSummary: false,
    });
  };

  handleShowData = () => {
    this.setState({ showSummary: true });
  };

  render() {
    return (
      <>
        <h1>Form Update Player</h1>
        <Text
          fieldName="username"
          placeholder="Input your username"
          onChange={this.handleFieldChange}
          value={this.state.username}
        />
        <br />
        <br />
        <Email
          fieldName="email"
          placeholder="Input your email"
          onChange={this.handleFieldChange}
          value={this.state.email}
        />
        <br />
        <br />
        <Experience
          fieldName="experience"
          placeholder="Input your experience"
          onChange={this.handleFieldChange}
          value={this.state.experience}
        />
        <br />
        <br />
        <Lvl
          fieldName="lvl"
          placeholder="Input your level"
          onChange={this.handleFieldChange}
          value={this.state.Lvl}
        />
        <br />
        <br />
        <button onClick={this.handleShowData}>Submit</button>
        <br />
        {this.state.showSummary && (
          <ul>
            <li>Username : {this.state.username}</li>
            <li>Email : {this.state.email}</li>
            <li>Experience : {this.state.experience} years</li>
            <li>Lvl : {this.state.lvl}</li>
          </ul>
        )}
      </>
    );
  }
}

export default UpdatePlayer;
