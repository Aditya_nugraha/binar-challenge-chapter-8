import Text from "../components/Text";
import Email from "../components/Email";
import Password from "../components/Password";
import { Component } from "react";

class CreatePlayer extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    showSummary: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showSummary: false,
    });
  };

  handleShowData = () => {
    this.setState({ showSummary: true });
  };

  render() {
    return (
      <>
        <h1>Form Create Player</h1>
        <Text
          fieldName="username"
          placeholder="Input your username"
          onChange={this.handleFieldChange}
          value={this.state.username}
        />
        <br />
        <br />
        <Email
          fieldName="email"
          placeholder="Input your email"
          onChange={this.handleFieldChange}
          value={this.state.email}
        />
        <br />
        <br />
        <Password
          name="password"
          placeholder="Input your password"
          onChange={this.handleFieldChange}
          value={this.state.password}
        />
        <br />
        <br />
        <button onClick={this.handleShowData}>Submit</button>
        <br />
        {this.state.showSummary && (
          <ul>
            <li>Username : {this.state.username}</li>
            <li>Email : {this.state.email}</li>
          </ul>
        )}
      </>
    );
  }
}

export default CreatePlayer;
