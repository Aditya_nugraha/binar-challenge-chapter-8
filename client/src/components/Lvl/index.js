import React from "react";

function Lvl(props) {
  const { value, placeholder, fieldName, onChange } = props;

  return (
    <input
      type="lvl"
      onChange={onChange}
      name={fieldName}
      value={value}
      placeholder={placeholder}
    />
  );
}

export default Lvl;
