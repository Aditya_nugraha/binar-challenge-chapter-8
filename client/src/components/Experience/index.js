import React from "react";

function Experience(props) {
  const { value, placeholder, fieldName, onChange } = props;

  return (
    <input
      type="experience"
      onChange={onChange}
      name={fieldName}
      value={value}
      placeholder={placeholder}
    />
  );
}

export default Experience;
