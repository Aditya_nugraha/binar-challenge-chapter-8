import React from "react";

function Email(props) {
  const { value, placeholder, fieldName, onChange } = props;

  return (
    <input
      type="email"
      onChange={onChange}
      name={fieldName}
      value={value}
      placeholder={placeholder}
    />
  );
}

export default Email;
