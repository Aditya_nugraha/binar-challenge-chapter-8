import "./App.css";
import { Component } from "react";
import CreatePlayer from "./pages/CreatePlayer";
import UpdatePlayer from "./pages/UpdatePlayer";
import ListPlayer from "./pages/ListPlayer";

class App extends Component {
  state = {
    activeMenu: "",
  };

  player = {
    username: "adit",
  };

  handleMenuChange = (event) => {
    this.setState({ activeMenu: event.target.id });
  };

  render() {
    return (
      <div className="App">
        <button id="createPlayer" onClick={this.handleMenuChange}>
          Create Player
        </button>
        <button id="updatePlayer" onClick={this.handleMenuChange}>
          Update Player
        </button>
        <button id="listPlayer" onClick={this.handleMenuChange}>
          List Player
        </button>
        <br />
        {this.state.activeMenu === "createPlayer" && <CreatePlayer />}
        {this.state.activeMenu === "updatePlayer" && (
          <UpdatePlayer player={this.player} />
        )}
        {this.state.activeMenu === "listPlayer" && <ListPlayer />}
      </div>
    );
  }
}

export default App;
